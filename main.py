#!/usr/bin/env python
'''Wrapper script that manages multiple installed projects/tools'''

PYTHON_VERSION = '2.7'

import os
import shlex
from argh import add_commands, dispatch
from argparse import ArgumentParser

ROOT_DIR = os.path.realpath(os.path.dirname(__file__))

commands = []
env = {}


def prepare():
    os.environ['PACKAGE_ROOT_DIR'] = ROOT_DIR

    # setup virtualenv
    os.environ['PATH'] = ':'.join([
        os.path.join(ROOT_DIR, '.virtualenv', 'bin'),
        os.environ['PATH'],
    ])
    os.environ['PYTHONPATH'] = os.path.join(ROOT_DIR, '.virtualenv', 'lib',
            'python' + PYTHON_VERSION, 'site-packages')


def ansible(inventory='custom/inventory.yml', *args):
    prepare()

    if len(args) == 1:
        args = shlex.split(args[0])

    binargs = ['ansible', '-i', inventory] + list(args)
    os.execvp(binargs[0], binargs)
commands.append(ansible)


def play(inventory='custom/inventory.yml', *args):
    prepare()

    if len(args) == 0:
        args = ['custom/site.yml']
    elif len(args) == 1:
        args = shlex.split(args[0])

    binargs = ['ansible-playbook', '-i', inventory] + list(args)
    os.execvp(binargs[0], binargs)
commands.append(play)


def pip(*args):
    prepare()

    if len(args) == 1:
        args = shlex.split(args[0])

    binargs = ['pip'] + list(args)
    os.execvp(binargs[0], binargs)
commands.append(pip)


if __name__ == '__main__':
    parser = ArgumentParser()
    add_commands(parser, commands)
    dispatch(parser)
