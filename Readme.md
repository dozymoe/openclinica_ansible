OpenClinica Installation Helper
-------------------------------

CAUTION: The setup is not meant for production use.

This project helps install the four projects related to OpenClinica: that is
OpenClinica, OpenClinica Participate, Rules Designer, and Enketo Forms.

Supported target operating system is Centos 6, Centos 7, Ubuntu Server 12,
Ubuntu Server 14.

We uses [Ansible][1], provisioning platform written in python, it's a tool like
Puppet or Chef, helps you setup remote server, multiple remote servers,
according to a preset configuration.

You have the freedom to install all projects to a single linux box, install all
project to multiple linux boxes, seperate each project to their own linux box,
in any combination and number of linux boxes.

There are two entry point files: inventory.yml and site.yml.  
They will be used like this:

    ./main.py play --inventory=inventory.yml site1.yml site2.yml

inventory.yml is where you list (or group), your linux boxes.

site.yml contains mapping of hosts and their roles, in our case roles are each
projects (OpenClinica, OpenClinica Participate, Rules Designer, Enketo Forms).

So we decide what project will be installed where, in site.yml.


Linux box requirements (The Target)
===================================

Supported OS is Centos 6, Centos 7, Ubuntu Server 12, Ubuntu Server 14, it is
possible to support other operating systems, like Gentoo, Debian, etc. They do
add complexity.

Needs:

  - ssh  
    Preferably allow root access using private ssh key file.

  - python2.7  
    Ansible requires python v2.7, the Ansible version used is v2.0.0.

We store the information about the server and ssh credentials in inventory.yml,
we can group them and act like they were a cluster, creating perfect copies.

[Ansible documentation on inventory][2].

Store your inventory.yml with a unique filename identifier in /custom/ directory,
under the project repository. Or anywhere outside the project repository, really,
reason is so they won't be overwritten when updating the repository.

You need to run main.py like so:

    ./main.py play --inventory=custom/MY_CUSTOM_INVENTORY.yml site.yml

Here the value of inventory argument is a relative path from current working
directory to inventory.yml.

The usual routine when creating ubuntu virtualbox is:

1. Create new vm using ubuntu server iso
2. Set root password, enable ssh service
3. Set network interface with static ip address
4. From host with this project installed, run `ssh-copy-id -i custom/ssh_keys/vboxoc root@IP_ADDRESS`
5. Run `./main.py play`


User box requirements (The Ansible installation)
================================================

Linux box is expected, it might be possible to use Windows, not tested.

Run `./configure.sh`, this should install python v2.7 and Ansible v2.0.0.

To use `main.py` we need python-argh and python-yaml ubuntu packages installed.

Run `./main.py --inventory=YOUR_INVENTORY.yml YOUR_SITE.yml YOUR_OTHER_SITE.yml`.

"YOUR_SITE.yml" should list custom-variables.yml file in `vars_files` list item,
see examples/site.yml for example, and examples/variables.yml.

What does variables.yml do? It contains key-value pairs. Like what branch to
checkout from OpenClinica Participate repository, etc, etc.

[Ansible documentation on variables][3]


Services scripts
================

The four projects of OpenClinica are installed as [runit services][4] in
"~/service/PROJECT_ID" (somewhat common practice to supervise micro services)
and controlled with the application `sv`.  
Their log files are available as "~/service/PROJECT_ID/current".

For example you can restart the tomcat service with this command: `sv -w120
restart ~/service/tomcat` (-w indicates the wait time/timeout).

You can monitor all log files with `tail -f ~/logs/*/current`, they are also
exported by nfs and samba.

Runit scripts are simple scripts that does not depends on PID files.


OpenClinica Participate notes
=============================

It is recommended to run Participate's spring-boot from the runit script,
because the settings are stored in the shell variables in the script.

For example you can run `~/service/PARTICIPATE_ID/run surefire:test` or
`~/service/PARTICIPATE_ID/run -Dspring.upload_root=/tmp failsafe:integration-test`.  
But make sure to turn off the service first, if needed, with
`sv -w120 stop ~/service/PARTICIPATE_ID`.


Changelog
=========

 *  2016-01-04  Role apt has been deprecated in favor of role package_manager.

 *  2016-01-04  Added support for Centos 6, Centos 7, Ubuntu 12, Ubuntu 14. OS will
    automatically be detected by Ansible, no configuration changes needed.



[1]: http://www.ansible.com/
[2]: http://docs.ansible.com/ansible/intro_inventory.html
[3]: http://docs.ansible.com/ansible/playbooks_variables.html
[4]: https://en.wikipedia.org/wiki/Runit
